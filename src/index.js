import React from 'react';

import {Provider} from 'react-redux';
import {createStore} from 'redux';
import rootReducer from './rootReducer';
import TodoList from './components/todoList';

const store = createStore(rootReducer);

const Index = () => {
  return (
    <Provider store={store}>
      <TodoList />
    </Provider>
  );
};

export default Index;
