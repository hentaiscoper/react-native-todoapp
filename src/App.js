import React from 'react';
import {View, StatusBar, StyleSheet} from 'react-native';
import Index from './index';

const App = () => {
  return (
    <>
      <StatusBar style={styles.statusBar} />
      <Index />
    </>
  );
};

const styles = StyleSheet.create({
  statusBar: {
    backgroundColor: 'red',
  },
});

export default App;
