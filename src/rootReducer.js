const initialState = {
  todoList: [
    {
      id: 0,
      completed: false,
      title: 'example',
      type: 1,
    },
    {
      id: 1,
      completed: true,
      title: 'example',
      type: 2,
    },
    {
      id: 3,
      completed: true,
      title: 'example',
      type: 3,
    },
    {
      id: 4,
      completed: true,
      title: 'example',
      type: 1,
    },
    // {
    //   id: 5,
    //   completed: true,
    //   title: 'example',
    //   type: 0,
    // },
    // {
    //   id: 6,
    //   completed: true,
    //   title: 'example',
    //   type: 0,
    // },
    // {
    //   id: 7,
    //   completed: true,
    //   title: 'example',
    //   type: 0,
    // },
    // {
    //   id: 8,
    //   completed: true,
    //   title: 'example',
    //   type: 0,
    // },
    // {
    //   id: 9,
    //   completed: true,
    //   title: 'example',
    //   type: 0,
    // },
    // {
    //   id: 10,
    //   completed: true,
    //   title: 'example',
    //   type: 0,
    // },
    // {
    //   id: 11,
    //   completed: true,
    //   title: 'example',
    //   type: 0,
    // },
    // {
    //   id: 12,
    //   completed: true,
    //   title: 'example',
    //   type: 0,
    // },
    // {
    //   id: 13,
    //   completed: true,
    //   title: 'example',
    //   type: 0,
    // },
    // {
    //   id: 14,
    //   completed: true,
    //   title: 'example',
    //   type: 0,
    // },
    // {
    //   id: 15,
    //   completed: true,
    //   title: 'example',
    //   type: 0,
    // },
    // {
    //   id: 16,
    //   completed: true,
    //   title: 'example',
    //   type: 0,
    // },
    // {
    //   id: 17,
    //   completed: true,
    //   title: 'example',
    //   type: 0,
    // },
  ],
  completeFilter: 0,
  typeFilter: 0,
  currentItem: 17,
};

/*
  TODO EXAMPLE:
  todo: {
    id: 0
    completed: false,
    title: 'example',
    type: 0
  }
 */

export default (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_TODO':
      return {
        ...state,
        todoList: [
          ...state.todoList,
          {...action.payload, completed: false, id: state.currentItem},
        ],
        currentItem: state.currentItem + 1,
      };
    case 'DELETE_TODO':
      console.log(action);
      return {
        ...state,
        todoList: [
          ...state.todoList.filter(todo => {
            if (todo.id !== action.id) {
              return {...todo};
            }
          }),
        ],
      };
    case 'TOGGLE_TODO':
      return {
        ...state,
        todoList: state.todoList.map(todo => {
          if (todo.id === action.id) {
            return {...todo, completed: !todo.completed};
          }
          return {...todo};
        }),
      };
    case 'SET_COMPLETE_FILTER':
      return {...state, completeFilter: action.id};
    case 'SET_TYPE_FILTER':
      return {...state, typeFilter: action.id};
    default:
      return {...state};
  }
};
