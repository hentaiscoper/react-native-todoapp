import React from 'react';
import {View, StyleSheet, Dimensions} from 'react-native';
import {
  Container,
  List,
  Header,
  Content,
  Title,
  Button,
  Text,
  Picker,
} from 'native-base';
import {connect} from 'react-redux';

import TodoItem from './todoItem';
import AddTodoModal from './addTodoModal';
import {flags} from '../constants';
import AddButton from "./addButton";

const {width} = Dimensions.get('window');

const TodoList = ({
  todoList,
  typeFilter,
  completeFilter,
  toggleTodo,
  deleteTodo,
  setCompleteFilter,
  setTypeFilter,
}) => {
  const renderTodoListByType = () => {
    switch (typeFilter) {
      case 0:
        return todoList;
      case 1:
        return todoList.filter(item => item.type === 1);
      case 2:
        return todoList.filter(item => item.type === 2);
      case 3:
        return todoList.filter(item => item.type === 3);
    }
  };

  const renderTodoListByComplete = () => {
    switch (completeFilter) {
      case 0:
        return renderTodoListByType().map((item, index) => (
          <TodoItem
            key={index}
            todo={item}
            toggle={() => toggleTodo(item.id)}
            deleteTodo={() => deleteTodo(item.id)}
          />
        ));
      case 1:
        return renderTodoListByType()
          .filter(item => {
            if (item.completed === false) {
              return {...item};
            }
          })
          .map((item, index) => (
            <TodoItem
              key={index}
              todo={item}
              toggle={() => toggleTodo(item.id)}
              deleteTodo={() => deleteTodo(item.id)}
            />
          ));
      case 2:
        return renderTodoListByType()
          .filter(item => {
            if (item.completed === true) {
              return {...item};
            }
          })
          .map((item, index) => (
            <TodoItem
              key={index}
              todo={item}
              toggle={() => toggleTodo(item.id)}
              deleteTodo={() => deleteTodo(item.id)}
            />
          ));
      default:
        break;
    }
  };

  const setBorder = id => completeFilter === id;

  const renderTypesPicker = () => {
    return flags.map((item, index) => (
      <Picker.Item key={index} label={item.name} value={item.id} />
    ));
  };

  return (
    <Container>
      <Header style={styles.header}>
        <Title>To-Do App</Title>
      </Header>
      <Content contentContainerStyle={styles.content}>
        <View>
          <View>
            <Picker
              itemStyle={styles.pickerItem}
              mode="dropdown"
              placeholder="Select tasks type"
              selectedValue={typeFilter}
              onValueChange={value => setTypeFilter(value)}>
              {renderTypesPicker()}
            </Picker>
          </View>
          <List style={styles.list}>
            {renderTodoListByComplete().length > 0 ? (
              renderTodoListByComplete()
            ) : (
              <View style={styles.helperText}>
                <Text>There are no tasks for the selected parameters.</Text>
              </View>
            )}
          </List>
          <View style={styles.completeFilter}>
            <Button
              transparent
              onPress={() => setCompleteFilter(0)}
              bordered={setBorder(0)}>
              <Text>All</Text>
            </Button>
            <Button
              transparent
              onPress={() => setCompleteFilter(1)}
              bordered={setBorder(1)}>
              <Text>Active</Text>
            </Button>
            <Button
              transparent
              onPress={() => setCompleteFilter(2)}
              bordered={setBorder(2)}>
              <Text>Completed</Text>
            </Button>
          </View>
        </View>
      </Content>
      <AddButton />
    </Container>
  );
};

const styles = StyleSheet.create({
  header: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {},
  helperText: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    color: 'white',
  },
  text: {
    color: 'black',
  },
  completeFilter: {
    marginTop: 10,
    marginBottom: 10,
    flexDirection: 'row',
    alignSelf: 'center',
    width,
    justifyContent: 'space-around',
  },
  pickerItem: {
    width,
  },
});

export default connect(
  state => ({
    todoList: state.todoList,
    completeFilter: state.completeFilter,
    typeFilter: state.typeFilter,
    isModalOpen: state.isModalOpen,
  }),
  dispatch => ({
    toggleTodo: id => {
      dispatch({type: 'TOGGLE_TODO', id});
    },
    deleteTodo: id => {
      dispatch({type: 'DELETE_TODO', id});
    },
    setCompleteFilter: id => {
      dispatch({type: 'SET_COMPLETE_FILTER', id});
    },
    setTypeFilter: id => {
      dispatch({type: 'SET_TYPE_FILTER', id});
    },
  }),
)(TodoList);
