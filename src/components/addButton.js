import React, {useState} from 'react';
import {Fab, Button, Icon} from 'native-base';
import AddTodoModal from './addTodoModal';

const AddButton = () => {
  const [active, setActive] = useState(false);

  return (
    <Fab
      style={{backgroundColor: '#5067FF'}}
      direction="up"
      active={active}
      onPress={() => setActive(!active)}>
      <Icon name={'add'} />
      <AddTodoModal />
      <Button />
    </Fab>
  );
};

export default AddButton;
