import React from 'react';

import {StyleSheet} from 'react-native';

import {Text, ListItem, CheckBox, Icon, View} from 'native-base';

const TodoItem = ({todo, toggle, deleteTodo}) => {
  const renderFlagIcon = () => {
    switch (todo.type) {
      case 1:
        return <Icon style={{color: 'green', marginLeft: 20}} name="md-home" />;
      case 2:
        return <Icon style={{color: 'red', marginLeft: 20}} name="md-laptop" />;
      case 3:
        return (
          <Icon style={{color: 'orange', marginLeft: 20}} name="md-book" />
        );
      default:
        break;
    }
  };

  return (
    <ListItem noIndent style={styles.listItem}>
      <View style={styles.iconGroup}>
        <CheckBox color="#3F51B5" checked={todo.completed} onPress={toggle} />
        {renderFlagIcon()}
      </View>
      <Text style={styles.text}>{todo.title}</Text>
      <Icon style={styles.icon} name="md-trash" onPress={deleteTodo} />
    </ListItem>
  );
};

const styles = StyleSheet.create({
  iconGroup: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  text: {
    alignSelf: 'center',
  },
  listItem: {
    flex: 1,
    justifyContent: 'space-between',
  },
  icon: {},
});

export default TodoItem;
