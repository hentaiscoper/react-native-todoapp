import React, {useState} from 'react';
import {Modal, Dimensions, StyleSheet} from 'react-native';
import {
  View,
  Text,
  Form,
  Item,
  Input,
  Label,
  Picker,
  Button,
  CheckBox,
  Icon,
  Body,
  Right,
} from 'native-base';
import {flags} from '../constants';
import {connect} from 'react-redux';
import {addTodo} from '../actions';

const {width} = Dimensions.get('window');

const AddTodoModal = ({addTodo}) => {
  const [isVisible, setIsVisible] = useState(false);
  const [name, setName] = useState('');
  const [flag, setFlag] = useState(1);

  const closeModal = () => {
    setFlag(1);
    setName('');
    setIsVisible(false);
  };

  const addNewTodo = () => {
    if (name !== '') {
      addTodo({
        title: name,
        type: flag,
      });
      closeModal();
    } else {
      alert('Field task name can`t be blank');
    }
  };

  const renderTypesPicker = () => {
    return flags.map((item, index) => {
      if (item.id !== 0) {
        return <Picker.Item key={index} label={item.name} value={item.id} />;
      }
    });
  };

  return (
    <View>
      <Modal animationType="slide" visible={isVisible}>
        <View>
          <View style={styles.header}>
            <Body>
              <Text style={styles.title}>Add ToDo</Text>
            </Body>
            <Right>
              <Button transparent onPress={() => closeModal()}>
                <Icon name="md-close" />
              </Button>
            </Right>
          </View>
          <Form style={styles.form}>
            <Item style={styles.formItem}>
              <Label style={styles.label}>Task name</Label>
              <Input
                style={styles.styledInput}
                value={name}
                onChangeText={text => setName(text)}
              />
            </Item>
            <Item style={styles.formItem} picker>
              <Label style={styles.label}>Task type</Label>
              <Picker
                style={styles.styledInput}
                mode="dropdown"
                placeholder="Select task type"
                selectedValue={flag - 1}
                onValueChange={value => {
                  setFlag(value);
                }}>
                {renderTypesPicker()}
              </Picker>
            </Item>
            <Button style={styles.addTodoButton} onPress={() => addNewTodo()}>
              <Text>Add ToDo</Text>
            </Button>
          </Form>
        </View>
      </Modal>
      <View style={styles.control}>
        <Button
          style={styles.addButton}
          rounded
          onPress={() => setIsVisible(true)}>
          <Icon name="md-add" />
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    height: 50,
    borderBottomWidth: 1,
  },
  title: {
    alignSelf: 'center',
  },
  form: {
    width,
    flex: 1,
    flexDirection: 'column',
  },
  formItem: {
    marginLeft: 0,
    height: 50,
    width,
  },
  label: {
    width: '40%',
    paddingLeft: 10,
  },
  styledInput: {
    width,
  },
  control: {
    flex: 1,
    alignItems: 'flex-end',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  addButton: {
    marginBottom: 20,
    marginRight: 20,
  },
  addTodoButton: {
    marginTop: 15,
    alignSelf: 'center',
  },
});

export default connect(
  null,
  dispatch => ({
    addTodo: payload => {
      dispatch({type: 'ADD_TODO', payload});
    },
  }),
)(AddTodoModal);
